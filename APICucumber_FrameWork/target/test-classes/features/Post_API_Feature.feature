Feature: Trigger the API 
@Post_API_TestCases
Scenario: Trigger the post API request with valid request parameter 
	Given Enter NAME and JOB in post request body 
	When Send the post request with Payload 
	Then Validate post Status Code 
	And Validate post response body parameters 