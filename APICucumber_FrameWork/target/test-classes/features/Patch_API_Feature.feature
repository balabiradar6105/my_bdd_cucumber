Feature: Trigger the API 

@Patch_API_TestCases	
Scenario: Trigger the patch API request with valid request parameter 
	Given Enter NAME and JOB in patch request body 
	When Send the patch request with Payload 
	Then Validate patch Status Code 
	And Validate patch response body parameters 