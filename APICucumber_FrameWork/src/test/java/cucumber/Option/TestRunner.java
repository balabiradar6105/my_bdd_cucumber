package cucumber.Option;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
@RunWith(Cucumber.class)

@CucumberOptions(features="src/test/java/features", glue= {"stepDefination"}, tags="@Post_API_TestCases or @Patch_API_TestCases")

public class TestRunner {

}
