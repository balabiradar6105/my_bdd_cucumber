package API_common_Methods;

import static io.restassured.RestAssured.given;

public class Common_Method_Handle_API {

	//------------------------------------------------------------- for post api-----------------------------------------------------------------

	public static int Post_statusCode(String requestbody, String endpoint) {
		int statusCode = given().header("Content-Type", "application/json").body(requestbody).when().post(endpoint)
				.then().extract().statusCode();
		return statusCode;
	}

	public static String Post_responsebody(String requestbody, String endpoint) {
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when().post(endpoint)
				.then().extract().response().asString();
		return responsebody;
	}
	//------------------------------------------------------ ------for put api-----------------------------------------------------------------------

	public static int Put_statusCode(String requestbody,String endpoint) {
		int statusCode=given().header("Content-Type", "application/json").body(requestbody)
				.when().post(endpoint).then().extract().statusCode();
		return statusCode;
		
	}
	
	public static String Put_responsebody(String requestbody, String endpoint) {
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when().post(endpoint)
				.then().extract().response().asString();
		return responsebody;
	}
	
	//-------------------------------------------------------------For patch api---------------------------------------------------------
	
	   public static int Patch_statusCode(String requestbody,String endpoint) {
		   int statusCode=given().header("Content-Type", "application/json").body(requestbody)
				   .when().patch(endpoint).then().extract().statusCode();
		   return statusCode;
		    }
	   public static String Patch_responsebody(String requestbody,String endpoint) {
		   String responsebody=given().header("Content-Type", "application/json").body(requestbody)
				   .when().patch(endpoint).then().extract().response().asString();
		   return responsebody;
	   }
	
	

	//-------------------------------------------------------------fot get api----------------------------------------------------------------------------------------

	public static int get_statusCode(String endpoint) {
		int statusCode = given().when().get(endpoint).then().extract().statusCode();
		return statusCode;
	}

	public static String get_responsebody(String endpoint) {
		String responsebody = given().when().get(endpoint).then().extract().response().asString();
		return responsebody;
	}

}
