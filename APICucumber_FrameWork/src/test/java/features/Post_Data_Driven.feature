Feature: Trigger the API 
Scenario Outline: Trigger the post API request with valid request parameter 
	Given Enter "<Name>" and "<Job>" in post request body 
	When Send the post request with data 
	Then Validate data_driven post Status Code 
	And Validate data_driven post response body parameters 
	
Examples:
        |Name|Job|
        |Prasad|QA|
        |Akshay|TL|