Feature: Trigger the API 
Scenario Outline: Trigger the put API request with valid request parameter 
	Given Enter "<Name>" and "<Job>" in put request body 
	When Send the put request with data 
	Then Validate data_driven put Status Code 
	And Validate data_driven put response body parameters 
	
	Examples: 
		|Name|Job|
		|Mayur|TM|
		|Sukanya|QA|	