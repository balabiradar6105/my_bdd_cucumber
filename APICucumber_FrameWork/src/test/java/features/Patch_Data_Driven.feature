Feature: Trigger the API 
Scenario Outline: Trigger the patch API request with valid request parameter 
	Given Enter "<Name>" and "<Job>" in patch request body 
	When Send the patch request with data 
	Then Validate data_driven patch Status Code 
	And Validate data_driven patch response body parameters
	
Examples:
        |Name|Job|
        |Krishna|QA|
        |Suyach|TL|