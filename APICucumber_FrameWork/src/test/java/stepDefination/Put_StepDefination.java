package stepDefination;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;

import API_common_Methods.Common_Method_Handle_API;
import Endpoint.Put_endpoint;
import Request_repository.Put_request_repository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_API_logs;
import utility_common_methods.Handle_directory;

public class Put_StepDefination {
	static File log_dir;
	static String requestbody;
	static String responsebody;
	static int statuscode;
	static String endpoint;
	
	@Given("Enter Name and Job in put request body")
	public void enter_name_and_job_in_put_request_body() throws IOException {
	    log_dir=Handle_directory.Create_log_directory("Put_TestCase.logs");
	    requestbody=Put_request_repository.Put_request_repository_TC2();
	    endpoint=Put_endpoint.Put_endpoint_TC2();
	}
	@When("send the put request with payload")
	public void send_the_put_request_with_payload() throws IOException {
		statuscode=Common_Method_Handle_API.Put_statusCode(requestbody, endpoint);
		responsebody=Common_Method_Handle_API.Put_responsebody(requestbody, endpoint);
		Handle_API_logs.evidence_creator(log_dir, "Put_TestCase", endpoint, requestbody, responsebody);

	}
	@Then("validate put statusCode")
	public void validate_put_status_code() {
	    Assert.assertEquals(statuscode, 201);
	}
	@Then("validate put responsebody parameters")
	public void validate_put_responsebody_parameters() {
	    JsonPath jsp_req=new JsonPath(requestbody);
	    String req_name=jsp_req.getString("name");
	    String req_job=jsp_req.getString("job");
	    JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		System.out.println("name is :" + res_name);
		String res_job = jsp_res.getString("job");
		System.out.println("job is :" + res_job);

		// step 4 validate the response
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
	}

}
