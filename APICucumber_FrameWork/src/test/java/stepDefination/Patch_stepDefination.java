package stepDefination;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_Methods.Common_Method_Handle_API;
import Endpoint.Patch_endpoint;
import Request_repository.Patch_request_repositry;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_API_logs;
import utility_common_methods.Handle_directory;


public class Patch_stepDefination {
	static File log_dir;
	static String requestbody;
	static String responsebody;
	static int statuscode;
	static String endpoint;
	@Given("Enter NAME and JOB in patch request body")
	public void enter_name_and_job_in_patch_request_body() throws IOException {
		log_dir = Handle_directory.Create_log_directory("Patch_TestCase.logs");
		requestbody = Patch_request_repositry.Patch_request_TC1();
		endpoint = Patch_endpoint.Patch_endpoint_TC2();   
	}
	@When("Send the patch request with Payload")
	public void send_the_patch_request_with_payload() throws IOException {
		statuscode = Common_Method_Handle_API.Patch_statusCode(requestbody, endpoint);
		responsebody = Common_Method_Handle_API.Patch_responsebody(requestbody, endpoint);
		Handle_API_logs.evidence_creator(log_dir, "Patch_TestCase", endpoint, requestbody, responsebody);
	}
	@Then("Validate patch Status Code")
	public void validate_patch_status_code() {

		Assert.assertEquals(statuscode, 200);
	}
	@Then("Validate patch response body parameters")
	public void validate_patch_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		System.out.println("name is :" + res_name);
		String res_job = jsp_res.getString("job");
		System.out.println("job is :" + res_job);

		// step 4 validate the response
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
	}

}
