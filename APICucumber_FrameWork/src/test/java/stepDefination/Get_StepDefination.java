package stepDefination;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.AssertJUnit;

import API_common_Methods.Common_Method_Handle_API;
import Endpoint.Get_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_API_logs;
import utility_common_methods.Handle_directory;

public class Get_StepDefination {
	static File log_dir;
	static String responsebody;
	static int statuscode;
	static String endpoint;

	@Given("Enter Name and Job in get request body")
	public void enter_name_and_job_in_get_request_body() {
		log_dir = Handle_directory.Create_log_directory("Get_TestCase.logs");
		endpoint = Get_endpoint.Get_endpoint_TC3();

	}

	@When("send the get request with payload")
	public void send_the_get_request_with_payload() throws IOException {
		statuscode = Common_Method_Handle_API.get_statusCode(endpoint);
		responsebody = Common_Method_Handle_API.get_responsebody(endpoint);
		Handle_API_logs.evidence_creator(log_dir, "Get_TestCase", endpoint, null, responsebody);

	}

	@Then("validate get statusCode")
	public void validate_get_status_code() {
		Assert.assertEquals(statuscode, 200);

	}

	@Then("validate get responsebody parameters")
	public void validate_get_responsebody_parameters() {
		int expected_id[] = { 7, 8, 9, 10, 11, 12 };
		String expected_firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String expected_lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String expected_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		JsonPath jsp_res = new JsonPath(responsebody);
		JSONObject array_res = new JSONObject(responsebody);
		JSONArray dataarray = array_res.getJSONArray("data");
		System.out.println(dataarray);
		int count = dataarray.length();
		System.out.println(count);
		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			System.out.println(res_id);
			int exp_id = expected_id[i];
			String res_firstname = dataarray.getJSONObject(i).getString("first_name");
			String exp_firstname = expected_firstname[i];
			String res_lastname = dataarray.getJSONObject(i).getString("last_name");
			String exp_lastname = expected_lastname[i];
			String res_email = dataarray.getJSONObject(i).getString("email");
			String exp_email = expected_email[i];

			AssertJUnit.assertEquals(res_id, exp_id);
			AssertJUnit.assertEquals(res_firstname, exp_firstname);
			AssertJUnit.assertEquals(res_lastname, exp_lastname);
			AssertJUnit.assertEquals(res_email, exp_email);

		}

	}
}
