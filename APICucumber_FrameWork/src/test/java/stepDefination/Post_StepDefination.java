package stepDefination;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_Methods.Common_Method_Handle_API;
import Endpoint.Post_endpoint;
import Request_repository.Post_request_repository;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_API_logs;
import utility_common_methods.Handle_directory;

public class Post_StepDefination {
	static File log_dir;
	static String endpoint;
	static String requestbody;
	static String responsebody;
	static int statusCode;
	@Before()
	public void setup() {
		System.out.println("Triggering process Start here");
	}

	@Given("Enter NAME and JOB in post request body")
	public void enter_name_and_job_in_post_request_body() throws IOException {
		log_dir = Handle_directory.Create_log_directory("Post_TestCase.logs");
		requestbody = Post_request_repository.Post_request_TC1();
		endpoint = Post_endpoint.Post_endpoint_TC1();
	}

	@When("Send the post request with Payload")
	public void send_the_post_request_with_payload() throws IOException {
		statusCode = Common_Method_Handle_API.Post_statusCode(requestbody, endpoint);
		responsebody = Common_Method_Handle_API.Post_responsebody(requestbody, endpoint);
		Handle_API_logs.evidence_creator(log_dir, "Post_TestCase", endpoint, requestbody, responsebody);
	}

	@Then("Validate post Status Code")
	public void validate_post_status_code() {
		Assert.assertEquals(statusCode, 201);
	}

	@Then("Validate post response body parameters")
	public void validate_post_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		System.out.println("name is :" + res_name);
		String res_job = jsp_res.getString("job");
		System.out.println("job is :" + res_job);

		// step 4 validate the response
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		

	}
	@After()
	public void teardown() {
		System.out.println("Process ended here");
	}

}
