package stepDefination;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_Methods.Common_Method_Handle_API;
import Endpoint.Patch_endpoint;
import Endpoint.Put_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_API_logs;
import utility_common_methods.Handle_directory;

public class Patch_Data_Driven {
	static File log_dir;
	static String endpoint;
	static String requestbody;
	static String responsebody;
	static int statusCode;
	@Given("Enter {string} and {string} in patch request body")
	public void enter_and_in_patch_request_body(String req_name, String req_job) {
		log_dir = Handle_directory.Create_log_directory("Patch_TestCase.logs");
		requestbody="{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
		endpoint=Patch_endpoint.Patch_endpoint_TC2(); 
	}
	@When("Send the patch request with data")
	public void send_the_patch_request_with_data() throws IOException {
		statusCode = Common_Method_Handle_API.Patch_statusCode(requestbody, endpoint);
		responsebody = Common_Method_Handle_API.Patch_responsebody(requestbody, endpoint);
		Handle_API_logs.evidence_creator(log_dir, "Patch_TestCase", endpoint, requestbody, responsebody);
	    
	}
	@Then("Validate data_driven patch Status Code")
	public void validate_data_driven_patch_status_code() {
		Assert.assertEquals(statusCode, 200);
    
	}
	@Then("Validate data_driven patch response body parameters")
	public void validate_data_driven_patch_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		System.out.println("name is :" + res_name);
		String res_job = jsp_res.getString("job");
		System.out.println("job is :" + res_job);

		// step 4 validate the response
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
	}


}
