package Request_repository;

import java.io.IOException;
import java.util.ArrayList;

import utility_common_methods.Excel_data_extractor;

public class Post_request_repository {
	
	public static String Post_request_TC1() throws IOException {
		ArrayList<String> Data =Excel_data_extractor.Excel_data_reader("Test_Data", "Post_API", "Post_TC1");
		//System.out.println(Data);
		String name=Data.get(1);
		String job=Data.get(2);
		String requestbody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
		return requestbody;
	}

}
